<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"  media="screen,projection"/>
    <style>
      a {
        color: unset;
      }
    </style>
  </head>

  <body>
    <nav>
        <div class="nav-wrapper">
            <a href="#" class="brand-logo center">File Manager</a>
        </div>
    </nav>
    <ul class="collection">
      <?php
        $sortedData = array();
        $folderToScan = ".";
        foreach(scandir($folderToScan) as $file) {
          if($file != '.' && $file != '..') {
            if (!is_dir("$folderToScan/$file")) {
              array_push($sortedData, "<li class=\"collection-item\"><a href=\"$folderToScan/$file\">$file</a></li>"); // add to end
            } else {
              array_unshift($sortedData, "<li class=\"collection-item\"><a href=\"$folderToScan/$file\"><b>$file</b></a></li>"); // add to beginning
            }
          }
        }
        foreach ($sortedData as $out) {
          echo $out;
        }
      ?>
    </ul>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </body>
</html>